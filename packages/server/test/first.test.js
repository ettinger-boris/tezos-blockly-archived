const { Builder, By, Key, until } = require('selenium-webdriver')
require('selenium-webdriver/chrome')
require('selenium-webdriver/firefox')
require('chromedriver')
require('geckodriver')

const { exec } = require('child_process');


const waitUntilTime = 20000

async function querySelector(selector, driver) {
  const el = await driver.wait(
    until.elementLocated(By.css(selector)),
    waitUntilTime
  )
  return await driver.wait(until.elementIsVisible(el), waitUntilTime)
}

const rootURL = 'https://www.mozilla.org/en-US/'
const url = 'http://localhost:5555';
let driver
let server
jasmine.DEFAULT_TIMEOUT_INTERVAL = 1000 * 60 * 5

beforeAll(async () => {
  driver = await new Builder().forBrowser('chrome').build()
		process.env.PORT = 5555;
		server = exec('yarn start',
		{ cwd:'../../',
		});
})

afterAll(async () => driver.quit())

test('initialises the context', async () => {
	console.log(server)
  await driver.get(url)
})

test('should click on navbar button to display a drawer', async () => {
  const anchor = await querySelector('[href=\'/en-US/firefox/\']', driver)
  const actual = await anchor.getText()
  const expected = 'Firefox'
  expect(actual).toEqual(expected)
})

afterAll(async () => {
	if (server) {
		server.kill();
	}
})
