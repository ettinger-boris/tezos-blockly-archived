function getRandomInt(max=100) {
	return Math.floor(Math.random() * Math.floor(max));
}

function sanitizeNameEntrypoint(block) {
	if (block.type == 'entry_point') {
		const name = block.getFieldValue('ENTRY_POINT_NAME'); 
		if (!name) {
			return `NO_NAME_${getRandomInt(100)}`
		} else {
			const bad_chars = /[^\d\w~]/gi;
			return name.replace(bad_chars, getRandomInt(100));
		}
	} else {
		return `NO_NAME_${getRandomInt(100)}`;
	}
}


function types([varsNames, includeStorage]) {
	let varList = varsNames.map(() => 'int');
	if (includeStorage) {
		varList.push('storageType')
	}
	return varList;
}

function names([varsNames, includeStorage]) {
	if (includeStorage) {
		return [...varsNames, 'storage'];
	}
	return varsNames;
}

function addNoOps(code, noOpsNeeded=false) {
	if (noOpsNeeded){
		return `(noOps, ${code})`;
	}
	return code;
}

function remArr(value) {
	if (Array.isArray(value)) {
		return value[0];
	}
	return value;
}

export { getRandomInt, sanitizeNameEntrypoint, types, names, addNoOps, remArr }

