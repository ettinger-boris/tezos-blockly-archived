/**
 * @fileoverview Generating ReLIGO for logic blocks.
 * @author q.neutron@gmail.com (Quynh Neutron)
 */
'use strict';
export default (Cls) => {
	return class extends Cls {
	logic_compare(block) {
  // Comparison operator.
  var OPERATORS = {
    'EQ': '==',
    'NEQ': '!=',
    'LT': '<',
    'LTE': '<=',
    'GT': '>',
    'GTE': '>='
  };
  var operator = OPERATORS[block.getFieldValue('OP')];
  var order = (operator == '==' || operator == '!=') ?
      this.ORDER_EQUALITY : this.ORDER_RELATIONAL;
  var argument0 = this.valueToCode(block, 'A', order) || '0';
  var argument1 = this.valueToCode(block, 'B', order) || '0';
  var code = argument0 + ' ' + operator + ' ' + argument1;
  return [code, order];
}
	logic_boolean(block) {
  // Boolean values true and false.
  var code = (block.getFieldValue('BOOL') == 'TRUE') ? 'true' : 'false';
  return [code, this.ORDER_ATOMIC];
}

	logic_ternary(block, addNoOps=false) {
  // Ternary operator.
  var value_if = this.valueToCode(block, 'IF',
      this.ORDER_CONDITIONAL) || 'false';
  var value_then = this.valueToCode(block, 'THEN',
      this.ORDER_CONDITIONAL) || 'null';
  var value_else = this.valueToCode(block, 'ELSE',
      this.ORDER_CONDITIONAL) || 'null';
  var code = '{ if (' + value_if + ') {' + value_then + ';} else {' + value_else + ';}}';
  return [code, this.ORDER_CONDITIONAL];
}
	logic_operation(block) {
  // Operations 'and', 'or'.
  var operator = (block.getFieldValue('OP') == 'AND') ? '&&' : '||';
  var order = (operator == '&&') ? this.ORDER_LOGICAL_AND :
      this.ORDER_LOGICAL_OR;
  var argument0 = this.valueToCode(block, 'A', order);
  var argument1 = this.valueToCode(block, 'B', order);
  if (!argument0 && !argument1) {
    // If there are no arguments, then the return value is false.
    argument0 = 'false';
    argument1 = 'false';
  } else {
    // Single missing arguments have no effect on the return value.
    var defaultArgument = (operator == '&&') ? 'true' : 'false';
    if (!argument0) {
      argument0 = defaultArgument;
    }
    if (!argument1) {
      argument1 = defaultArgument;
    }
  }
  var code = argument0 + ' ' + operator + ' ' + argument1;
  return [code, order];
};


}
}
