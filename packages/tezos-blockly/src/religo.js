/**
 * @license
 * Copyright 2012 Google LLC
 * SPDX-License-Identifier: Apache-2.0
 */

/**
 * @fileoverview Helper functions for generating ReLIGO for blocks.
 * @author fraser@google.com (Neil Fraser)
 */
'use strict';

import * as Blockly from 'blockly/core';
import logicMixin from './religo/logic.js';
import mathMixin from './religo/math.js';
import varMixin from './religo/variables.js';
import tezosMixin from './religo/tezos.js';
import { getRandomInt, sanitizeNameEntrypoint, types, names, addNoOps, remArr } from './code-utils.js';
/**
 * Religo code generator.
 * @type {!Blockly.Generator}
 */


class ReLIGOGenerator extends tezosMixin(logicMixin(varMixin(mathMixin(Blockly.Generator)))) {
	init(workspace) {
		// Create a dictionary of definitions to be printed before the code.
		this.definitions_ = Object.create(null);
		// Create a dictionary mapping desired function names in definitions_
		// to actual function names (to avoid collisions with user functions).
		this.functionNames_ = Object.create(null);

		if (!this.variableDB_) {
			this.variableDB_ =
				new Blockly.Names(this.RESERVED_WORDS_);
		} else {
			this.variableDB_.reset();
		}

		this.variableDB_.setVariableMap(workspace.getVariableMap());

		var defvars = [];
		// Add developer variables (not created or named by the user).
		var devVarList = Blockly.Variables.allDeveloperVariables(workspace);
		for (var i = 0; i < devVarList.length; i++) {
			defvars.push(this.variableDB_.getName(devVarList[i],
				Blockly.Names.DEVELOPER_VARIABLE_TYPE));
		}

		// Add user variables, but only ones that are being used.
		var variables = Blockly.Variables.allUsedVarModels(workspace);
		for (var i = 0; i < variables.length; i++) {
			defvars.push(this.variableDB_.getName(variables[i].getId(),
				Blockly.VARIABLE_CATEGORY_NAME));
		}

		// Declare all of the variables.
		if (defvars.length) {
			this.definitions_['variables'] =
				'var ' + defvars.join(', ') + ';';
		}
	};

	noOpsNeeded(block) {
		if (!block) {
			return false;
		}
		switch(block.type) {
			case 'return_type':
				return block.getInputTargetBlock('OPERATIONS') == null;
			case 'logic_ternary':
				const thenBlock = block.getInputTargetBlock('THEN');
				const elseBlock = block.getInputTargetBlock('ELSE');
				return this.noOpsNeeded(thenBlock) || this.noOpsNeeded(elseBlock);
			default:
				return true;
		}
	}

getVarNames(block) {
	const desc = block.getDescendants();
	const varBlocks = desc.filter(block => block.type === 'variables_get');
	const varNames = varBlocks.map(block =>  this.variableDB_.getName(block.getFieldValue('VAR'),
		Blockly.VARIABLE_CATEGORY_NAME));
	const strBlock = desc.filter(block => block.type === 'storage');
	return [[...new Set(varNames)], strBlock.length > 0];
}

	payToNeeded(block) {
		if (!block) {
			return false;
		}
		switch(block.type) {
			case 'return_type':
				return block.getInputTargetBlock('OPERATIONS') !== null;
			case 'logic_ternary':
				const thenBlock = block.getInputTargetBlock('THEN');
				const elseBlock = block.getInputTargetBlock('ELSE');
				return this.payToNeeded(thenBlock) || this.payToNeeded(elseBlock);
			default:
				return false;
		}

	}

	blockToCodeNoOps(block, addNoOps=false) {
		if (!addNoOps || block.type === 'return_type') {
			return this.blockToCode(block);
		}
		switch(block.type) {
			case 'logic_ternary':
				const thenBlock = block.getInputTargetBlock('THEN');
				const elseBlock = block.getInputTargetBlock('ELSE');
				const noOpsThen = this.noOpsNeeded(thenBlock);
				const noOpsElse = this.noOpsNeeded(elseBlock);
				if (noOpsThen && noOpsElse) {
					return `(noOps, ${remArr(this.blockToCode(block))})`;
				}
				const value_if = remArr(this.valueToCode(block, 'IF',
					this.ORDER_CONDITIONAL)) || 'false';
				const value_then = remArr(this.valueToCode(block, 'THEN',
					this.ORDER_CONDITIONAL)) || 'null';
				const value_else = remArr(this.valueToCode(block, 'ELSE',
					this.ORDER_CONDITIONAL)) || 'null';
				return  '{ if (' + value_if + ') {' 
					+ this.addNoOps(value_then, noOpsThen) 
					+ ';} else {' 
					+ this.addNoOps(value_else, noOpsElse) + ';}}';
			default:
				let code = this.blockToCode(block);
				if (Array.isArray(code)) {
					code = code[0];
				}
				return `(noOps, ${code})`;
		}

	}

	entryPointCode(name, vars, block) {
		let code = this.blockToCodeNoOps(block, this.noOpsNeeded(block));
		if (Array.isArray(code)) {
			code = code[0];
		};
		let nm = names(vars).join(', ');
		let typ = types(vars).join(', ');
		if (nm.length == 0) {
			nm = `placeHolder${getRandomInt()}`;
			typ = 'unit';
		}
		return `let ${name.toLowerCase()} = ((${nm}):(${typ})): return 
			=> ${code};\n`;
	}

	//there seems to be a bug in Generator.prototype.blockToCode
	// so reimplementing
	blockToCode(block, opt_thisOnly) {
		if (!block ) {
			return '';
		}
		 if (!block.isEnabled()) {
    // Skip past this block if it is disabled.
    return opt_thisOnly ? '' : this.blockToCode(block.getNextBlock());
  }
  if (block.isInsertionMarker()) {
    // Skip past insertion markers.
    return opt_thisOnly ? '' : this.blockToCode(block.getChildren(false)[0]);
  }

		 var func = this[block.type];
  if (typeof func != 'function') {
    throw Error('Language "' + this.name_ + '" does not know how to generate ' +
        ' code for block type "' + block.type + '".');
  }
	// BUG in Blockly's code, in the original code it's func.call(block, block)
  // First argument to func.call is the value of 'this' in the generator.
  // Prior to 24 September 2013 'this' was the only way to access the block.
  // The current preferred method of accessing the block is through the second
  // argument to func.call, which becomes the first parameter to the generator.
  var code = func.call(this, block);
  if (Array.isArray(code)) {
    // Value blocks return tuples of code and operator order.
    if (!block.outputConnection) {
      throw TypeError('Expecting string from statement block: ' + block.type);
    }
    return [this.scrub_(block, code[0], opt_thisOnly), code[1]];
  } else if (typeof code == 'string') {
    if (this.STATEMENT_PREFIX && !block.suppressPrefixSuffix) {
      code = this.injectId(this.STATEMENT_PREFIX, block) + code;
    }
    if (this.STATEMENT_SUFFIX && !block.suppressPrefixSuffix) {
      code = code + this.injectId(this.STATEMENT_SUFFIX, block);
    }
    return this.scrub_(block, code, opt_thisOnly);
  } else if (code === null) {
    // Block has handled code generation itself.
    return '';
  } else {
    throw SyntaxError('Invalid code generated: ' + code);
  }


	}

	workspaceToCode(workspace) {
		if (!workspace) {
			// Backwards compatibility from before there could be multiple workspaces.
			console.warn('No workspace specified in workspaceToCode call.  Guessing.');
			workspace = Blockly.getMainWorkspace();
		}
		this.init(workspace);
		const blocks = workspace.getTopBlocks(true);
		const varNames = blocks.map(block => this.getVarNames(block));
		const varArgs = varNames.map(names);
		const entryPointNames = blocks.map(sanitizeNameEntrypoint);
		const preamble = 'type storageType = int;\n\ntype return = (list (operation), storageType);\n\n';
		const actionVariantsForTypes = entryPointNames.map((name, i) => {
			const nm = name.toUpperCase();
			const actionVars = varNames[i][0];
			const ln = actionVars.length;
			switch(ln){
				case 0:
					return nm;
				case 1:
					return `${nm} (int)`;
				default:
					return `${nm} ((${actionVars.map(()=> 'int')}))`;
			}
		});
		const actionVariantsForFunctions = entryPointNames.map((name, i) => {
			const actionVars = varNames[i][0];
			if (actionVars.length > 1) {
				return `${name.toUpperCase()} tuple`;
			} else if (actionVars.length == 1) {
				return `${name.toUpperCase()} ${actionVars[0]}`;
			}

			return name.toUpperCase();
		});
		const actionType =`type actionType = \n${actionVariantsForTypes.join('\n|')};\n`;
		const entryPointMain = blocks.map((block) => {		
			if (block.type == 'entry_point') {
				return block.getInputTargetBlock('ENTRY_POINT_VALUE');
			}
			return block;
		});
		const codes = entryPointMain.map((block, index) => {
			return this.entryPointCode(entryPointNames[index], varNames[index], block);
		});
		const noOpsNeeded = entryPointMain.map(block => this.noOpsNeeded(block)).reduce((a,b) => a||b, false);
		const noOpsStatement = noOpsNeeded? 'let noOps = ([]: list(operation));\n\n': '';
		const payToNeeded = entryPointMain.map(block => this.payToNeeded(block)).reduce((a,b) => a||b, false);
		const payToStatement = payToNeeded? `let payTo = ((dst, amt): (address, tez)) : operation => {
			let contract = switch (Tezos.get_contract_opt(dst) : option(contract(unit))) {
				| Some(contract) => contract
					| None => (failwith("no contract") : contract(unit))
			};
			Tezos.transaction(unit, amt, contract);
		}
		\n`: '';
		const variantsCallFunctions = actionVariantsForFunctions.map((variant, index) => {
			const fnName = entryPointNames[index].toLowerCase();
			let rhs;
			switch(varNames[index][0].length){
				case 0:
					rhs = `${fnName}(${varArgs[index]})`;
					break;
				case 1:
					if (varArgs[index].length == 1) {
						rhs = `${fnName}(${varArgs[index]})`;
					} else {
						rhs = `${fnName}((${varArgs[index]}))`;
					}
					break;
				default:
					let nm = varNames[index][0].join(', ');
					rhs = `{
						let (${nm}) = tuple;
						${fnName}((${varArgs[index].join(', ')}));
					}`;
			}

			return `|${variant} => ${rhs}\n`;
		});
		const main = `let main = ((action, storage) : (actionType, storageType)) : return => {
			switch (action) {
				${variantsCallFunctions.join('\n')}};}`;
		this.finish();
		return `${preamble}\n${actionType}\n${noOpsStatement}\n${payToStatement}\n\n${codes.join('\n')}\n\n${main}`;
	}

	finish(code) {
		// Clean up temporary data.
		delete this.definitions_;
		delete this.functionNames_;
	}

	/**
	 * Encode a string as a properly escaped ReLIGO string, complete with
	 * quotes.
	 * @param {string} string Text to encode.
	 * @return {string} ReLIGO string.
	 * @private
	 */
	quote_(string) {
		// Can't use goog.string.quote since Google's style guide recommends
		// JS string literals use single quotes.
		string = string.replace(/\\/g, '\\\\')
			.replace(/\n/g, '\\\n')
			.replace(/'/g, '\\\'');
		return '\'' + string + '\'';
	};

	/**
	 * Encode a string as a properly escaped multiline ReLIGO string, complete
	 * with quotes.
	 * @param {string} string Text to encode.
	 * @return {string} ReLIGO string.
	 * @private
	 */
	multiline_quote_(string) {
		// Can't use goog.string.quote since Google's style guide recommends
		// JS string literals use single quotes.
		var lines = string.split(/\n/g).map(this.quote_);
		return lines.join(' + \'\\n\' +\n');
	};

	/**
	 * Common tasks for generating ReLIGO from blocks.
	 * Handles comments for the specified block and any connected value blocks.
	 * Calls any statements following this block.
	 * @param {!Blockly.Block} block The current block.
	 * @param {string} code The ReLIGO code created for this block.
	 * @param {boolean=} opt_thisOnly True to generate code for only this statement.
	 * @return {string} ReLIGO code with comments and subsequent blocks added.
	 * @private
	 */
	scrub_(block, code, opt_thisOnly) {
		var commentCode = '';
		// Only collect comments for blocks that aren't inline.
		if (!block.outputConnection || !block.outputConnection.targetConnection) {
			// Collect comment for this block.
			var comment = block.getCommentText();
			if (comment) {
				comment = Blockly.utils.string.wrap(comment,
					this.COMMENT_WRAP - 3);
				commentCode += this.prefixLines(comment + '\n', '// ');
			}
			// Collect comments for all value arguments.
			// Don't collect comments for nested statements.
			for (var i = 0; i < block.inputList.length; i++) {
				if (block.inputList[i].type == Blockly.INPUT_VALUE) {
					var childBlock = block.inputList[i].connection.targetBlock();
					if (childBlock) {
						comment = this.allNestedComments(childBlock);
						if (comment) {
							commentCode += this.prefixLines(comment, '// ');
						}
					}
				}
			}
		}
		var nextBlock = block.nextConnection && block.nextConnection.targetBlock();
		var nextCode = opt_thisOnly ? '' : this.blockToCode(nextBlock);
		return commentCode + code + nextCode;
	};

	/**
	 * Gets a property and adjusts the value while taking into account indexing.
	 * @param {!Blockly.Block} block The block.
	 * @param {string} atId The property ID of the element to get.
	 * @param {number=} opt_delta Value to add.
	 * @param {boolean=} opt_negate Whether to negate the value.
	 * @param {number=} opt_order The highest order acting on this value.
	 * @return {string|number}
	 */
	getAdjusted(block, atId, opt_delta, opt_negate,
		opt_order) {
		var delta = opt_delta || 0;
		var order = opt_order || this.ORDER_NONE;
		if (block.workspace.options.oneBasedIndex) {
			delta--;
		}
		var defaultAtIndex = block.workspace.options.oneBasedIndex ? '1' : '0';
		if (delta > 0) {
			var at = this.valueToCode(block, atId,
				this.ORDER_ADDITION) || defaultAtIndex;
		} else if (delta < 0) {
			var at = this.valueToCode(block, atId,
				this.ORDER_SUBTRACTION) || defaultAtIndex;
		} else if (opt_negate) {
			var at = this.valueToCode(block, atId,
				this.ORDER_UNARY_NEGATION) || defaultAtIndex;
		} else {
			var at = this.valueToCode(block, atId, order) ||
				defaultAtIndex;
		}

		if (Blockly.isNumber(at)) {
			// If the index is a naked number, adjust it right now.
			at = Number(at) + delta;
			if (opt_negate) {
				at = -at;
			}
		} else {
			// If the index is dynamic, adjust it in code.
			if (delta > 0) {
				at = at + ' + ' + delta;
				var innerOrder = this.ORDER_ADDITION;
			} else if (delta < 0) {
				at = at + ' - ' + -delta;
				var innerOrder = this.ORDER_SUBTRACTION;
			}
			if (opt_negate) {
				if (delta) {
					at = '-(' + at + ')';
				} else {
					at = '-' + at;
				}
				var innerOrder = this.ORDER_UNARY_NEGATION;
			}
			innerOrder = Math.floor(innerOrder);
			order = Math.floor(order);
			if (innerOrder && order >= innerOrder) {
				at = '(' + at + ')';
			}
		}
		return at;
	};
}
const ReLIGO = new ReLIGOGenerator('ReLIGO');

ReLIGO.addReservedWords(

	// https://developer.mozilla.org/en-US/docs/Web/ReLIGO/Reference/Lexical_grammar#Keywords
	'break,case,catch,class,const,continue,debugger,default,delete,do,else,export,extends,finally,for,function,if,import,in,instanceof,new,return,super,switch,this,throw,try,typeof,var,void,while,with,yield,' +
	'enum,' +
	'implements,interface,let,package,private,protected,public,static,' +
	'await,' +
	'null,true,false,' +
	// Magic variable.
	'arguments,' +
	// Everything in the current environment (835 items in Chrome, 104 in Node).
	Object.getOwnPropertyNames(Blockly.utils.global).join(','));

/**
 * Order of operation ENUMs.
 * https://developer.mozilla.org/en/ReLIGO/Reference/Operators/Operator_Precedence
 */
ReLIGO.ORDER_ATOMIC = 0;           // 0 "" ...
ReLIGO.ORDER_MEMBER = 1.2;         // . []
ReLIGO.ORDER_FUNCTION_CALL = 2;    // ()
ReLIGO.ORDER_BITWISE_NOT = 4.1;    // ~
ReLIGO.ORDER_UNARY_PLUS = 4.2;     // +
ReLIGO.ORDER_UNARY_NEGATION = 4.3; // -
ReLIGO.ORDER_LOGICAL_NOT = 4.4;    // !
ReLIGO.ORDER_EXPONENTIATION = 5.0; // **
ReLIGO.ORDER_MULTIPLICATION = 5.1; // *
ReLIGO.ORDER_DIVISION = 5.2;       // /
ReLIGO.ORDER_MODULUS = 5.3;        // %
ReLIGO.ORDER_SUBTRACTION = 6.1;    // -
ReLIGO.ORDER_ADDITION = 6.2;       // +
ReLIGO.ORDER_BITWISE_SHIFT = 7;    // << >> >>>
ReLIGO.ORDER_RELATIONAL = 8;       // < <= > >=
ReLIGO.ORDER_EQUALITY = 9;         // == != === !==
ReLIGO.ORDER_BITWISE_AND = 10;     // &
ReLIGO.ORDER_BITWISE_XOR = 11;     // ^
ReLIGO.ORDER_BITWISE_OR = 12;      // |
ReLIGO.ORDER_LOGICAL_AND = 13;     // &&
ReLIGO.ORDER_LOGICAL_OR = 14;      // ||
ReLIGO.ORDER_CONDITIONAL = 15;     // ?:
ReLIGO.ORDER_ASSIGNMENT = 16;      // = += -= **= *= /= %= <<= >>= ...
ReLIGO.ORDER_COMMA = 18;           // ,
ReLIGO.ORDER_NONE = 99;            // (...)

/**
 * List of outer-inner pairings that do NOT require parentheses.
 * @type {!Array.<!Array.<number>>}
 */
ReLIGO.ORDER_OVERRIDES = [
	// (foo()).bar -> foo().bar
	// (foo())[0] -> foo()[0]
	[ReLIGO.ORDER_FUNCTION_CALL, ReLIGO.ORDER_MEMBER],
	// (foo())() -> foo()()
	[ReLIGO.ORDER_FUNCTION_CALL, ReLIGO.ORDER_FUNCTION_CALL],
	// (foo.bar).baz -> foo.bar.baz
	// (foo.bar)[0] -> foo.bar[0]
	// (foo[0]).bar -> foo[0].bar
	// (foo[0])[1] -> foo[0][1]
	[ReLIGO.ORDER_MEMBER, ReLIGO.ORDER_MEMBER],
	// (foo.bar)() -> foo.bar()
	// (foo[0])() -> foo[0]()
	[ReLIGO.ORDER_MEMBER, ReLIGO.ORDER_FUNCTION_CALL],

	// !(!foo) -> !!foo
	[ReLIGO.ORDER_LOGICAL_NOT, ReLIGO.ORDER_LOGICAL_NOT],
	// a * (b * c) -> a * b * c
	[ReLIGO.ORDER_MULTIPLICATION, ReLIGO.ORDER_MULTIPLICATION],
	// a + (b + c) -> a + b + c
	[ReLIGO.ORDER_ADDITION,ReLIGO.ORDER_ADDITION],
	// a && (b && c) -> a && b && c
	[ReLIGO.ORDER_LOGICAL_AND, ReLIGO.ORDER_LOGICAL_AND],
	// a || (b || c) -> a || b || c
	[ReLIGO.ORDER_LOGICAL_OR, ReLIGO.ORDER_LOGICAL_OR]
];

export default ReLIGO;
